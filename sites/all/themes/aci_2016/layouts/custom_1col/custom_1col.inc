<?php

/**
 * Implements hook_panels_layouts()
 */
function aci_2016_custom_1col_panels_layouts() {
  $items['custom_1col'] = array(
    'title' => t('1 column ZF'),
    'category' => t('CUSTOM 2016'),
    'icon' => 'custom_1col.png',
    'theme' => 'custom_1col',
    //'admin css' => '../foundation_panels_admin.css',
    'regions' => array(
      'left' => t('Left'),
      'off_left' => t('Off-canvas left'),
    ),
  );
  return $items;
}

