<?php

/**
 * Implements hook_panels_layouts()
 */
function aci_2016_custom_2col_panels_layouts() {
  $items['custom_2col'] = array(
    'title' => t('2 columns ZF'),
    'category' => t('CUSTOM 2016'),
    'icon' => 'custom_2col.png',
    'theme' => 'custom_2col',
    //'admin css' => '../foundation_panels_admin.css',
    'regions' => array(
      'left' => t('Left'),
      'right' => t('Right'),
      'top' => t('Top'),
      'bottom' => t('Bottom'),
    ),
  );
  return $items;
}

