<?php
/**
 * @file
 * Template for an Image Zoom thumbnail image.
 *
 * Available variables:
 * - $items: Thumbnails array
 */
?>

<div class="easyzoom-thumbnails-wrapper">
  <?php foreach ($items as $item): ?>
    <div class="easyzoom-thumbnails">
      <a href="<?php print $item['#zoom_image_url']; ?>"
         data-standard="<?php print $item['#image_url']; ?>">
        <?php print $item['#thumbnail']; ?>
      </a>
    </div>
  <?php endforeach; ?>
</div>
